package salariati.test.model;

import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.exception.EmployeeException;
import salariati.model.Employee;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class EmployeeTest {

    private static final int line = 2;

    @Test
    public void testGetEmployeeFromString_01(){
        String _employee = "Suciu;Radu;1970424125791;ASISTENT";
        try{
            Employee.getEmployeeFromString(_employee, line);
            fail();
        }catch (EmployeeException ex){
            assertEquals(ex.getMessage(), "Invalid line at: " + line);
        }
    }

    @Test
    public void testGetEmployeeFromString_02(){
        String _employee = "Suciu;Radu;1970424125791;nimic;3000";
        try{
            Employee.getEmployeeFromString(_employee, line);
            fail();
        }catch (EmployeeException ex){
            assertEquals(ex.getMessage(), "Invalid line at: " + line);
        }
    }

    @Test
    public void testGetEmployeeFromString_03() throws EmployeeException {
        String _employee = "Suciu;Radu;1970424125791;ASISTENT;3000";
        Employee employee = Employee.getEmployeeFromString(_employee, line);
        assertEquals(DidacticFunction.ASISTENT, employee.getFunction());
    }

    @Test
    public void testGetEmployeeFromString_04() throws EmployeeException {
        String _employee = "Suciu;Radu;1970424125791;LECTURER;3000";
        Employee employee = Employee.getEmployeeFromString(_employee, line);
        assertEquals(DidacticFunction.LECTURER, employee.getFunction());
    }

    @Test
    public void testGetEmployeeFromString_05() throws EmployeeException {
        String _employee = "Suciu;Radu;1970424125791;TEACHER;3000";
        Employee employee = Employee.getEmployeeFromString(_employee, line);
        assertEquals(DidacticFunction.TEACHER, employee.getFunction());
    }

    @Test
    public void testGetEmployeeFromString_06() throws EmployeeException {
        String _employee = "Suciu;Radu;1970424125791;CONFERENTIAR;3000";
        Employee employee = Employee.getEmployeeFromString(_employee, line);
        assertEquals(DidacticFunction.CONFERENTIAR, employee.getFunction());
    }

}