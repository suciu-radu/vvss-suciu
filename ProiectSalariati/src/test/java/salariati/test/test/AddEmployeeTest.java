package salariati.test.test;

import static org.junit.Assert.*;

import org.junit.rules.ExpectedException;
import salariati.exception.EmployeeException;
import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.service.EmployeeService;
import salariati.enumeration.DidacticFunction;

public class AddEmployeeTest {

	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeService controller;
	public ExpectedException exception = ExpectedException.none();


	@Before
	public void setUp() {
		employeeRepository = new EmployeeMock();
		controller         = new EmployeeService(employeeRepository);
	}


	@Test
	public void testEc1() throws Exception {
		int oldSize = controller.getEmployeesList().size();
		controller.addEmployee("Ion","Aurel","1970424125791",DidacticFunction.ASISTENT,3000);
		int newSize = controller.getEmployeesList().size();
		assertEquals(oldSize+1, newSize);
	}

	@Test
	public void testEc2(){
		try {
			controller.addEmployee("Spaniolu","Aurel","1970424125791",DidacticFunction.LECTURER,0);
			fail();
		} catch (Exception e) {
			assertEquals("Datele angajatului nu sunt valide!",e.getMessage());
		}
	}


	@Test
	public void testEc5(){
		try {
			controller.addEmployee("Aurel","Ab","1970424123420",DidacticFunction.TEACHER,5000);
			fail();
		} catch (Exception e) {
			assertEquals("Datele angajatului nu sunt valide!",e.getMessage());
		}
	}

	@Test
	public void testBva1(){
		try {
			controller.addEmployee("Aurel","Ab","1970424123420",DidacticFunction.TEACHER,5000);
			fail();
		} catch (Exception e) {
			assertEquals("Datele angajatului nu sunt valide!",e.getMessage());
		}
	}
	@Test
	public void testBva7(){
		try {
			controller.addEmployee("Contantin","Aureliaaaaaaaaaaaaaaaaaa","1970424123420",DidacticFunction.ASISTENT,300);
			fail();
		} catch (Exception e) {
			assertEquals("Datele angajatului nu sunt valide!",e.getMessage());
		}
	}

	@Test
	public void testBva3() throws Exception {
		int oldSize = controller.getEmployeesList().size();
		controller.addEmployee("Aurel","Americaaaaaaaaaaaaaaanu","1970424123491",DidacticFunction.ASISTENT,3000);
		int newSize = controller.getEmployeesList().size();
		assertEquals(oldSize+1, newSize);

	}

	@Test
	public void testBva6() {
		try {
			controller.addEmployee("Ion","Aurel","1970424123491",DidacticFunction.ASISTENT,2);
		} catch (Exception e) {
			if(e instanceof EmployeeException){
				fail();
			}
		}
	}

}
